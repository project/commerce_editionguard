<?php

namespace Drupal\commerce_editionguard_reactions\Event;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\state_machine\Plugin\Workflow\WorkflowInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowTransition;

/**
 * Provides an event that carries an order and its editionguard transactions.
 */
class OrderEditionGuardTransactionsEvent extends WorkflowTransitionEvent {

  /**
   * The EditionGuard transactions associated with this order.
   *
   * @var \Drupal\editionguard\Entity\Transaction[]
   */
  protected $transactions;

  /**
   * Constructs a new OrderEditionGuardTransactionEvent.
   *
   * @param array $transactions
   *   The transactions of the order.
   * @param \Drupal\state_machine\Plugin\Workflow\WorkflowTransition $transition
   *   The transition.
   * @param \Drupal\state_machine\Plugin\Workflow\WorkflowInterface $workflow
   *   The workflow.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The state field name.
   */
  public function __construct(array $transactions, WorkflowTransition $transition, WorkflowInterface $workflow, ContentEntityInterface $entity, $field_name) {
    $this->transactions = $transactions;

    parent::__construct($transition, $workflow, $entity, $field_name);
  }

  /**
   * Get the editionguard transactions.
   *
   * @return array|\Drupal\editionguard\Entity\Transaction[]
   *   Returns array of entity transactions.
   */
  public function getTransactions() {
    return $this->transactions;
  }

}
