CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

 * For a full description of the module visit:
   https://www.drupal.org/project/commerce_editionguard

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/commerce_editionguard


REQUIREMENTS
------------

You will need to have
<a href="https://www.drupal.org/project/editionguard_api">EditionGuard API</a>,
<a href="https://www.drupal.org/project/editionguard">EditionGuard</a>
modules installed and configured.


INSTALLATION
------------

Install the EditionGuard API module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.

USAGE
-------------
1. Navigate to Administration > Commerce > Product variation types
2. Select the Product Variation to be used for the EditionGuard Books
3. Navigate Manage Fields
4. Add a new EditionGuard book field (located under Reference category)
5. Navigate to Administration > Commerce > Products
6. Select the product and edit its variation
7. Select the book from the reference field

The rest is automated.
When a customer purchases the Product Variation with the
EditionGuard book field it will trigger the event to request
a new transaction from EditionGuard API.

The customer can view and download the purchased books from their
user account in 'My purchased eBooks'.


MAINTAINERS
-----------

 * lexsoft - https://www.drupal.org/u/lexsoft
