<?php

namespace Drupal\commerce_editionguard\EventSubscriber;

use Drupal\commerce_editionguard_reactions\Event\OrderEditionGuardTransactionsEvent;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Trigger Referral Candy when the order transitions to Fulfillment.
 */
class OrderFulfillmentSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type composer.jsonmanager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The EditionGuard API Client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $editionGuard;

  /**
   * The corresponding request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new Event Subscriber object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api
   *   The EditionGuard client api.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The corresponding request stack.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, EditionGuardApiClientInterface $editionguard_api, RequestStack $request_stack, EventDispatcherInterface $event_dispatcher, ModuleHandlerInterface $module_handler) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->editionGuard = $editionguard_api;
    $this->requestStack = $request_stack;
    $this->eventDispatcher = $event_dispatcher;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // The format for adding a state machine event to subscribe to is:
    // {group}.{transition key}.pre_transition or
    // {group}.{transition key}.post_transition
    // depending on when you want to react.
    $events = [
      'commerce_order.place.pre_transition' => [
        'createEditionGuardTransaction',
        -100,
      ],
    ];
    return $events;
  }

  /**
   * Create EditionGuard Transaction.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createEditionGuardTransaction(WorkflowTransitionEvent $event) {
    // Get the order entity.
    $order = $event->getEntity();

    // Get customer details.
    $customer = $order->getCustomer();
    $email = $order->getEmail();
    $order_id = $order->id();
    $billing_profile = $order->getBillingProfile();
    if ($billing_profile && $billing_profile->address) {
      $address = $billing_profile->address->first();
      $first_name = $address->getGivenName();
      $last_name = $address->getFamilyName();
    }

    $transactions = [];
    // Get order items.
    foreach ($order->getItems() as $order_item) {

      // Get product variation.
      $product_variation = $order_item->getPurchasedEntity();
      $product_variation_title = $product_variation->getTitle();
      $quantity = $order_item->getQuantity();

      $definitions = $product_variation->getFieldDefinitions();
      foreach ($definitions as $field_type => $definition) {
        $definition_type = $definition->getType();

        // Check if editionguard_book_field exists in product variation.
        if ($definition_type === 'editionguard_book_field') {

          $field = $product_variation->get($field_type);
          if (!$field->isEmpty()) {
            $books = $field->getValue();

            foreach ($books as $book) {

              $book_id = $book['target_id'];
              $ebook_storage = $this->entityTypeManager->getStorage('editionguard_book');
              if (isset($book_id)) {

                /** @var \Drupal\editionguard\Entity\Book $editionguard_book */
                $editionguard_book = $ebook_storage->load($book_id);
                $resource_id = $editionguard_book->getResourceId();
                $drm_type = $editionguard_book->getDrmType();

                if (isset($resource_id)) {

                  // EditionGuard Api client.
                  $form_params = [
                    "resource_id" => $resource_id,
                    "show_instructions" => $book['show_instructions'],
                    "external_id" => $order_id,
                  ];

                  if (!empty($book['uses_remaining']) && $book['uses_remaining'] !== '0') {
                    $form_params["uses_remaining"] = $book['uses_remaining'];
                  }

                  // Watermark only works on books with DRM EditionMark.
                  if ($drm_type === '3' && $book['watermark_enable'] === '1' && isset($email)) {
                    $watermark = $book['watermark_enable'];
                    if (isset($first_name) && isset($last_name)) {
                      $customer_name = $first_name . ' ' . $last_name;
                    }
                    $form_params["watermark_name"] = $customer_name ?? $customer->label();
                    $form_params["watermark_email"] = $email;
                    $form_params["watermark_place_begin"] = $book['watermark_place_begin'];
                    $form_params["watermark_place_end"] = $book['watermark_place_end'];
                    $form_params["watermark_place_random"] = $book['watermark_place_random'];
                    $form_params["watermark_place_random_count"] = $book['watermark_place_random_count'];
                  }
                  $query_params = [];

                  // Loop quantity.
                  for ($i = 1; $i <= $quantity; $i++) {

                    $endpoint = $this->editionGuard->getEndpointPluginManager()
                      ->createInstance('transaction_create');
                    $request = $this->editionGuard->request($endpoint, $query_params, $form_params);
                    if (isset($request['id'])) {

                      /** @var \Drupal\editionguard\Entity\Transaction $transaction */
                      $transaction = $this->entityTypeManager->getStorage('editionguard_transaction')
                        ->create([
                          'user_id' => ['target_id' => $customer->id()],
                          'name' => $product_variation_title,
                          'book_id' => $book_id,
                          'transaction_id' => $request['id'],
                          'resource_id' => $request['resource_id'],
                          'external_id' => $request['external_id'],
                          'link' => $request['download_link'],
                          'show_instructions' => $request['show_instructions'],
                          'uses_remaining' => $values['uses_remaining'] ?? '',
                          'watermark' => $watermark ?? '0',
                        ]);
                      $transaction->save();
                      $transactions[$transaction->id()] = $transaction;
                    }
                    else {
                      $this->logger->error('Transaction Request failed for customer: @customer , order id: @order , product variation: @product_variation , with request:' . '<pre><code>' . print_r($request, TRUE) . '</code></pre>' . 'Please verify if the EdtionGuard API is working.', [
                        '@product_variation' => $product_variation_title,
                        '@customer' => $email,
                        '@order' => $order_id,
                      ]);
                    }
                  }
                }
              }
            }
          }
          else {
            $this->logger->info('eBook order failed for customer @customer , order id @order , product variation @product_variation . The ebook field: @field is empty, please add a book.', [
              '@field' => $field->getName(),
              '@product_variation' => $product_variation_title,
              '@customer' => $email,
              '@order' => $order_id,
            ]);
          }
        }
      }
    }

    // Dispatch the event used for commerce_reactions
    // if the submodule is enabled.
    if (!empty($transactions) && $this->moduleHandler->moduleExists('commerce_editionguard_reactions')) {
      $this->eventDispatcher->dispatch(new OrderEditionGuardTransactionsEvent(
        $transactions,
        $event->getTransition(),
        $event->getWorkflow(),
        $event->getEntity(),
        $event->getFieldName()
      ), 'commerce_editionguard_reactions.commerce_order.editionguard_transactions');
    }
  }

}
